﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp_loadexcel
{
    public class DBManager
    {
        private string _ConStrLocal = "server = 10.0.9.47;Database=FinanceTest;UID=sa;Password=a;";
        public string ConStrLocal
        {
            get { return _ConStrLocal; }
            set { _ConStrLocal = value; }
        }


        public string saveDB(string param1, string param2, string param3, string param4, string result)
        {
            string returnvalue = "";

            SqlConnection sqlcnn = new SqlConnection(ConStrLocal);
            sqlcnn.Open();

            SqlCommand cmd = new SqlCommand("BulkInsertTraining", sqlcnn);
            cmd.CommandType = CommandType.StoredProcedure;
            

            cmd.Parameters.Add("@param1", SqlDbType.VarChar).Value = param1;
            cmd.Parameters.Add("@param2", SqlDbType.VarChar).Value = param2;
            cmd.Parameters.Add("@param3", SqlDbType.VarChar).Value = param3;
            cmd.Parameters.Add("@param4", SqlDbType.VarChar).Value = param4;
            cmd.Parameters.Add("@result", SqlDbType.VarChar).Value = result;

            cmd.Parameters.Add("@return_value", SqlDbType.VarChar).Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();
            sqlcnn.Close();

            returnvalue = cmd.Parameters["@return_value"].Value.ToString();
            return returnvalue;
        }


        public string deleteTable()
        {
            string result = "";

            SqlConnection sqlcnn = new SqlConnection(ConStrLocal);
            sqlcnn.Open();

            SqlCommand cmd = new SqlCommand("delTraining", sqlcnn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@return_value", SqlDbType.VarChar).Direction = ParameterDirection.ReturnValue;

            cmd.ExecuteNonQuery();
            sqlcnn.Close();

            result = cmd.Parameters["@return_value"].Value.ToString();
            return result;
        }


        public DataTable GetListParameter(string commandsql)
        {
            DataTable dataTableParameter = new DataTable();

            SqlConnection cnn = new SqlConnection(ConStrLocal);
            cnn.Open();

            SqlCommand cmd = new SqlCommand(commandsql, cnn);
            cmd.CommandType = CommandType.StoredProcedure;
            
            

            SqlDataAdapter dataAdapterParam = new SqlDataAdapter(cmd);
            dataAdapterParam.Fill(dataTableParameter);

            cnn.Close();

            return dataTableParameter;
        }


        public DataTable TestData(string param1, string param2, string param3, string param4)
        {
            DataTable dataTableResult = new DataTable();

            SqlConnection cnn = new SqlConnection(ConStrLocal);
            cnn.Open();

            SqlCommand cmd = new SqlCommand("callProcML", cnn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@param1", SqlDbType.VarChar).Value = "["+param1+"]";
            cmd.Parameters.Add("@param2", SqlDbType.VarChar).Value = "[" + param2 + "]";
            cmd.Parameters.Add("@param3", SqlDbType.VarChar).Value = "[" + param3 + "]";
            cmd.Parameters.Add("@param4", SqlDbType.VarChar).Value = "[" + param4 + "]";


            SqlDataAdapter dataAdapterParam = new SqlDataAdapter(cmd);
            dataAdapterParam.Fill(dataTableResult);

            cnn.Close();

            return dataTableResult;
        }


    }
}
