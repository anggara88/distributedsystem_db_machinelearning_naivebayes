﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ExcelDataReader;
using DecisionTree;

namespace WindowsFormsApp_loadexcel
{
    public partial class Form1 : Form
    {

        DBManager dBManager = new DBManager();
        public Form1()
        {

            InitializeComponent();
        }



        DataTableCollection tableCollection;
        private void buttonFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = "Excel 97-2003 Workbook|*.xls|Excel Workbook|*.xlsx" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    textBoxFilename.Text = openFileDialog.FileName;
                    using (var stream = File.Open(openFileDialog.FileName, FileMode.Open, FileAccess.Read))
                    {
                        using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                        {
                            var result = reader.AsDataSet(new ExcelDataSetConfiguration()
                            {
                                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                                {
                                    UseHeaderRow = true
                                }
                            });
                            tableCollection = result.Tables;
                            comboBoxSheet.Items.Clear();
                            foreach (DataTable table in tableCollection)
                            {
                                comboBoxSheet.Items.Add(table.TableName);
                            }
                        }
                    }
                }
            }

        }

        private void comboBoxSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = tableCollection[comboBoxSheet.SelectedItem.ToString()];
            dataGridView1.DataSource = dt;
            deleteDB(dt);
            
        }

        private void buttonCreateTree_Click(object sender, EventArgs e)
        {

            string paramTest1 = this.comboBoxParam1.GetItemText(this.comboBoxParam1.SelectedItem);
            string paramTest2 = this.comboBoxParam2.GetItemText(this.comboBoxParam2.SelectedItem);
            string paramTest3 = this.comboBoxParam3.GetItemText(this.comboBoxParam3.SelectedItem);
            string paramTest4 = this.comboBoxParam4.GetItemText(this.comboBoxParam4.SelectedItem);

            MessageBox.Show(paramTest1 + "," + paramTest2 + "," + paramTest3 + "," + paramTest4);

            MessageBox.Show("[" + paramTest1 + "]" + "," + "[" + paramTest2 + "]" + "," + "[" + paramTest3 + "]" + "," + "[" + paramTest4 + "]");

            DataTable dataTableResult = dBManager.TestData(paramTest1, paramTest2, paramTest3, paramTest4);


            string columnName1 = dataTableResult.Columns[0].ColumnName;
            string columnName2 = dataTableResult.Columns[1].ColumnName;
            string result1 = dataTableResult.Rows[0][columnName1].ToString();
            string result2 = dataTableResult.Rows[0][columnName2].ToString();

            richTextBoxResult.Text = "";
            richTextBoxResult.Text += columnName1+":"+ result1;
            richTextBoxResult.Text += "\n";
            richTextBoxResult.Text += columnName2 +":"+ result2;

        }

       
        private static string ReadLineTrimmed()
        {
            return Console.ReadLine().TrimStart().TrimEnd();
        }

        private static void EndProgram()
        {
            Environment.Exit(0);
        }

        private static void DisplayErrorMessage(string errorMessage)
        {
            MessageBox.Show($"\n{errorMessage}\n");
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }


        public void deleteDB(DataTable dt)
        {
            string result = dBManager.deleteTable();
            if (result == "0")
            {
                //MessageBox.Show("Success delete table");
                saveToDb(dt);
            }
            else
            {
                MessageBox.Show("Failed insert detail invoice, please try again or contact IT department");
            }
        }

        public void saveToDb(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {

                string param1 = "";
                string param2 = "";
                string param3 = "";
                string param4 = "";
                string resultdb = "";

                
                for (int j = 0; j <= dt.Rows.Count - 1; j++)
                {
                    param1 = dt.Rows[j][0].ToString().Trim();
                    param2 = dt.Rows[j][1].ToString().Trim();
                    param3 = dt.Rows[j][2].ToString().Trim();
                    param4 = dt.Rows[j][3].ToString().Trim();
                    resultdb = dt.Rows[j][4].ToString().Trim();

                    string result = dBManager.saveDB(param1, param2, param3, param4, resultdb);
                    if (result != "0")
                    {
                        MessageBox.Show("Failed insert detail invoice, please try again or contact IT department");
                    }
                   
                }

                groupBoxTest.Enabled = true;

                GetDataParam();
            }
        }


        void GetDataParam()
        {
            DataTable dataTableListParameter = null;
            for (int i = 0; i< 4; i++)
            {
                int listkei = i + 1;

                dataTableListParameter = dBManager.GetListParameter("getparam" + listkei.ToString());
                isiDataSourceCombobox(dataTableListParameter, listkei);
            }
            
           
        }

        void isiDataSourceCombobox(DataTable dt,int paramke)
        {
            if (paramke == 1)
            {
                comboBoxParam1.DataSource = dt;
                comboBoxParam1.DisplayMember = "param1";
            }

            if (paramke == 2)
            {
                comboBoxParam2.DataSource = dt;
                comboBoxParam2.DisplayMember = "param2";
            }

            if (paramke == 3)
            {
                comboBoxParam3.DataSource = dt;
                comboBoxParam3.DisplayMember = "param3";
            }

            if (paramke == 4)
            {
                comboBoxParam4.DataSource = dt;
                comboBoxParam4.DisplayMember = "param4";
            }
        }


    }
}
