USE [FinanceTest]
GO
/****** Object:  StoredProcedure [dbo].[callProcML]    Script Date: 19/12/2019 4:02:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[callProcML] @param1 varchar(50), @param2 varchar(50), @param3 varchar(50), @param4 varchar(50)
as
-- exec callProcML ['<=30'], ['medium'], ['yes'], ['fair']
begin
DECLARE @RunStoredProcSQL VARCHAR(1000)
SET @RunStoredProcSQL = 'EXEC SalesTest.dbo.procML '+@param1+','+@param2+','+@param3+','+@param4+''
EXEC (@RunStoredProcSQL)  AT [10.0.9.42]
end