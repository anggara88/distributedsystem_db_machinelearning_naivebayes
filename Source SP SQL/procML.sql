USE [SalesTest]
GO
/****** Object:  StoredProcedure [dbo].[procML]    Script Date: 19/12/2019 4:01:54 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[procML] @param1 varchar(50), @param2 varchar(50), @param3 varchar(50), @param4 varchar(50)
as
begin
-- exec procml [Baik], [Baik], [IPS], [IPA]
declare @totalRow numeric(21,5)
declare @totalResult1 numeric(21,5)
declare @totalResult2 numeric(21,5)
declare @nilaiResult1 numeric(21,5)
declare @nilaiResult2 numeric(21,5)
declare @totParam1Res1 numeric(21,5)
declare @totParam1Res2 numeric(21,5)
declare @Param1Res1 numeric(21,5)
declare @Param1Res2 numeric(21,5)
declare @totParam2Res1 numeric(21,5)
declare @totParam2Res2 numeric(21,5)
declare @Param2Res1 numeric(21,5)
declare @Param2Res2 numeric(21,5)
declare @totParam3Res1 numeric(21,5)
declare @totParam3Res2 numeric(21,5)
declare @Param3Res1 numeric(21,5)
declare @Param3Res2 numeric(21,5)
declare @totParam4Res1 numeric(21,5)
declare @totParam4Res2 numeric(21,5)
declare @Param4Res1 numeric(21,5)
declare @Param4Res2 numeric(21,5)
declare @akumuluasiRes1 numeric(21,5)
declare @akumuluasiRes2 numeric(21,5)
declare @presentasiRes1 numeric(21,5)
declare @presentasiRes2 numeric(21,5)
declare @result1 varchar(50)
declare @result2 varchar(50)
declare @kolom1 varchar(50)
declare @kolom2 varchar(50)

select * into #tempTraining from [10.0.9.47].financetest.dbo.trainingml


set @result1 = (select top 1 result from #tempTraining order by result asc)
set @result2 = (select top 1 result from #tempTraining order by result desc)
set @kolom1 = 'presentasi_' + @result1
set @kolom2 = 'presentasi_' + @result2

print @kolom1
print @kolom2
print @result1
print @result2




--inisialisasi awal
set @totalRow = (select count(*) from #tempTraining )
set @totalResult1 = (select count(*) from #tempTraining  where result = @result1)
set @totalResult2 = (select count(*) from #tempTraining  where result = @result2)


set @nilaiResult1 = @totalResult1 / @totalRow
set @nilaiResult2 = @totalResult2 / @totalRow

-- mencari nilai param1
set @totParam1Res1 = (select count(*) from #tempTraining  where param1 = @param1 and result = @result1)
set @totParam1Res2 = (select count(*) from #tempTraining  where param1 = @param1 and result = @result2)
set @Param1Res1 = @totParam1Res1 / @totalResult1
set @Param1Res2 = @totParam1Res2 / @totalResult2


-- mencari nilai param2
set @totParam2Res1 = (select count(*) from #tempTraining  where param2 = @param2 and result = @result1)
set @totParam2Res2 = (select count(*) from #tempTraining  where param2 = @param2 and result = @result2)
set @Param2Res1 = @totParam2Res1 / @totalResult1
set @Param2Res2 = @totParam2Res2 / @totalResult2

-- mencari nilai param3
set @totParam3Res1 = (select count(*) from #tempTraining  where param3 = @param3 and result = @result1)
set @totParam3Res2 = (select count(*) from #tempTraining  where param3 = @param3 and result = @result2)
set @Param3Res1 = @totParam3Res1 / @totalResult1
set @Param3Res2 = @totParam3Res2 / @totalResult2

-- mencari nilai param4
set @totParam4Res1 = (select count(*) from #tempTraining  where param4 = @param4 and result = @result1)
set @totParam4Res2 = (select count(*) from #tempTraining  where param4 = @param4 and result = @result2)
set @Param4Res1 = @totParam4Res1 / @totalResult1
set @Param4Res2 = @totParam4Res2 / @totalResult2

-- nilai akumulasi yes & no
set @akumuluasiRes1 = @Param1Res1 * @Param2Res1 * @Param3Res1 * @Param4Res1
set @akumuluasiRes2 = @Param1Res2 * @Param2Res2 * @Param3Res2 * @Param4Res2

-- hitung presentasi yes & no
set @presentasiRes1 = @nilaiResult1 * @akumuluasiRes1
set @presentasiRes2 = @nilaiResult2 * @akumuluasiRes2

exec ('select ' + @presentasiRes1 + ' as ' + @kolom1 + ' , ' + @presentasiRes2 + ' as ' + @kolom2 )

end